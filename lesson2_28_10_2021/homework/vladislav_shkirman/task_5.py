# Task4**
# Create a car with color, model, current mileage, max mileage. By default max mileage=current mileage + 1000
# Car should have 2 methods:
# 1.print_info: which will print all information about car e.g “This is color model car”
# 2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
# If it is more or equals max mileage then car is broken and should print it
#
# e.g
# current mileage=8
# max mileage=12
# Result of the method with 2 kms
# My current mileage is 9
# My current mileage is 10
# Result of the method with 3 kms
# My current mileage is 11
# I’m broken because of my current mileage is maximum(12).

from CarClass_task_5 import Car

color = "green"
model = "Golf"
current_mileage = 278
max_mileage = 1000
kms = input("Enter how much mileage is passed: ")
Volkswagen = Car().car_info(color, model)
Volkswagen = Car().drive(kms, current_mileage, max_mileage)